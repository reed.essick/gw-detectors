# gw-detectors

A simple module that implements detector and network objects along with logic to compute antenna responses (including the full frequency-dependent response).