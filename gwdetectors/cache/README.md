Reference PSDs are taken from:

  * aligo-design.csv.gz   : https://dcc.ligo.org/LIGO-T1800044/public (aLIGODesign.txt)
  * advirgo-design.csv.gz : https://journals.aps.org/prd/abstract/10.1103/PhysRevD.96.084004 (Fig. 4)
  * aplus-design.csv.gz   : https://dcc.ligo.org/LIGO-T2000012/public (AplusDesign.txt)
  * ce-design.csv.gz      : https://journals.aps.org/prd/abstract/10.1103/PhysRevD.96.084004
    - Fig. 4 "base curve" that does not include fall-off in interferometric response at high frequencies
