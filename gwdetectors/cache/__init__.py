"""a module storing known detector orientations and PSDs
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from .orientation import *
from .psd import *
from .detector import *
