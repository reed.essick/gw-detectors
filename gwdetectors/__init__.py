"""a module that implements object-oriented representations of ground-based Gravitational-Wave detector networks
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from .io import *
from .detector import *
