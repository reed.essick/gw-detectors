"""a module housing detector classes, PSDs, etc
"""
__author__ = "Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

from .utils import *
from .twoarmdetector import *
